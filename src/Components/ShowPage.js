import axios from 'axios';
import React,{useEffect,useState} from 'react'
import { useParams } from 'react-router-dom';

function ShowPage() {
    const param = useParams();

    const [blog,setblog] = useState([]);
    const fetchData = async () => {
        const data = await axios.get(`http://localhost:8000/blogs/${param.id}`);
        return data.data;
    }

    useEffect( () => {
        let data = async () => {
            let data1 = await fetchData();
            setblog(data1);
        };
        data();
    },[])

  return (
    <div>
        <h1>{blog.author}</h1>
        <h2>{blog.title}</h2>
        <p>{blog.description}</p>
    </div>
  )
}

export default ShowPage