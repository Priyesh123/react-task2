import axios from 'axios';
import React,{useState,useEffect} from 'react'
import { useParams, useNavigate } from 'react-router-dom';

function Edit() {
    const param = useParams();
    console.log(param.id);
    const navigate = useNavigate();

    const[blog,setblog] = useState([]);
    const fetchData = async () => {
        const data = await axios.get(`http://localhost:8000/blogs/${param.id}`);
        return data.data;
    }
    console.log("Blog => ",blog);

    useEffect( () => {
        let data = async () => {
            let data1 = await fetchData();
            setblog(data1);
        };
        data();
    },[])

    const handlechange = e => {
        setblog({...blog,[e.target.name]: e.target.value})

    }

    const submithandler = async (e) => {
        e.preventDefault();
        await axios.patch('http://localhost:8000/blogs/'+blog.id ,blog);
        navigate(-1);
    }

    return (
        <div>
            <form onSubmit={submithandler}>
                Author : <input type="text" name='author' value={blog.author} onChange={handlechange} />
                <br/>
                Title : <input type="text" name='title' value={blog.title} onChange={handlechange}/>
                <br/>
                Description : <input type="text" name='description' value={blog.description} onChange={handlechange}/>
                <br/>
                <button type='submit'>Save</button>
            </form>
        </div>
    )
}

export default Edit