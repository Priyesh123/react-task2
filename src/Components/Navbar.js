import React from 'react'
import { Link } from 'react-router-dom';


function Navbar() {
  return (
    <div>
        <div style={{padding: "50px 100px", display: "inline-block"}}>
            <Link to='/contact' >Contact</Link> 
        </div>
        <div style={{padding: "50px 100px", display: "inline-block"}}>
            <Link to='/about' >About</Link>
        </div>
        <div style={{padding: "50px 100px", display: "inline-block"}}>
            <Link to='/home' >Home</Link>
        </div>
    </div>
  )
}

export default Navbar