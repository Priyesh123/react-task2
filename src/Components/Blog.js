import axios from 'axios';
import React,{useState,useEffect} from 'react'
import Home from './Home';
import ShowPage from './ShowPage';
import { useNavigate } from 'react-router-dom';
import Edit from './Edit';

function Blog() {
    const [blog,setblog] = useState([]);
    const navigate = useNavigate();

    const fetchData = async () => {
        const data = await axios.get('http://localhost:8000/blogs');
        return data.data;
    }

    useEffect( () => {
        let data = async () => {
            let data1 = await fetchData();
            setblog(data1);
        };
        data();
    },[])

    const handelDelete = (id) => {
        const newBlog = blog.filter(ele => (id !== ele.id) )
        setblog(newBlog);
        const deleteBlog = async () => {
            await axios.delete(`http://localhost:8000/blogs/${id}`);
        }
        deleteBlog();
    }

    const handleShow = (id) => {
        navigate(`/blog/${id}`);
    }

    const handleEdit = id => {
        navigate(`/blog/edit/${id}`);
    }

    return (
        <div>
            {
                blog.length && blog.map(ele => <Home key={ele.id} object={ele} function={[handelDelete,handleShow,handleEdit]}/>)
            }
        </div>
    );
}

export default Blog