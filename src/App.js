import './App.css';
import {Route,Routes} from 'react-router-dom';
import Blog from './Components/Blog';
import ShowPage from './Components/ShowPage';
import Edit from './Components/Edit';
import About from './Components/About';
import Contact from './Components/Contact';
import Navbar from './Components/Navbar';

function App() {
  return (
    <>
        <Navbar />
       <Routes>
        <Route path='/' element={ <Blog /> } />
        <Route path='/home' element={<Blog/>}/>
        <Route path='/blog/:id' element={<ShowPage/> } />
        <Route path='/blog/edit/:id' element={<Edit/> } />
        <Route path='/about' element={<About/> } />
        <Route path='/contact' element={<Contact/> } />

      </Routes>  
    </>
   );
}

export default App;
