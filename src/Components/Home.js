import React from 'react'

function Home(props) {
    return (
    <div>
        <h6> Id  : {props.object.id}</h6>
        <h4> title : {props.object.title}</h4>
        <h5>author : {props.object.author}</h5>
        <button onClick={()=>props.function[2](props.object.id)}>Edit</button>
        <button onClick={() => props.function[0](props.object.id)}>Delete</button>
        <button onClick={()=> props.function[1](props.object.id)}>Show</button>
    </div>
  )
}

export default Home